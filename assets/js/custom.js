$(document).ready(function () {
    $('#Form-primaryTabs a[data-toggle="tab"]').click(function (event) {
        localStorage.setItem("active-tab", $(this).attr('href'));
    });

    if (localStorage.getItem("active-tab")) {
        var element = $('#Form-primaryTabs a[href="' + localStorage.getItem("active-tab") + '"]');
        var dataTarget = element.attr("data-target");
        if (dataTarget) {
            var dataTarget = dataTarget.substr(1);
            $("#Form-primaryTabs .nav-tabs li").removeClass("active");
            $("#Form-primaryTabs .tab-content .tab-pane").removeClass("active");
            element.parent().addClass("active")
            $('#' + dataTarget).addClass("active")
        }
    }
});